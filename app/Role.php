<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    //

    public function users()
    {
        return $this->hasMany(User::class);
        //belongsTo
        //hasMany
        //hasOne
        //belongsToMany (en los dos)
        // ejemplo N:M productos con categorias
        // la tabla de la relación se deber llamar oblicatoriamente cathegory_product
        // alfabéticamente, cathegory antes que product.
        // product_cathegory no funcionaría
    }
}
